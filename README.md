**Xeroweb**

Docker container based on [baseimage kasmvnc](https://github.com/linuxserver/docker-baseimage-kasmvnc) of [Xerolinux](https://xerolinux.xyz/).  It provides a complete desktop experience for testing and other tasks.

**Disclaimer: This is a FAN project and is not affiliated with Xerolinux. Please refrain from seeking help on the main channels.** 

For assistance, please open an issue here or join my [Discord server](https://discord.gg/wFnRTjWgTM).

![Preview Image](https://i.ibb.co/Km65v93/Screenshot-20231121-180851.png)

**Usage**

docker-compose
```
---
version: "2.1"
services:
  webtop:
    image: notkudu/xeroweb
    container_name: xeroweb
    security_opt:
      - seccomp:unconfined #optional
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
    volumes:
      - /path/to/data:/config
      - /var/run/docker.sock:/var/run/docker.sock #optional
    ports:
      - 3000:3000
      - 3001:3001
    devices:
      - /dev/dri:/dev/dri #optional
    shm_size: "1gb" #optional
    restart: unless-stopped
```

docker cli
```
docker run -d \
  --name=xeroweb \
  --security-opt seccomp=unconfined `#optional` \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -p 3000:3000 \
  -p 3001:3001 \
  -v /path/to/data:/config \
  -v /var/run/docker.sock:/var/run/docker.sock `#optional` \
  --device /dev/dri:/dev/dri `#optional` \
  --shm-size="1gb" `#optional` \
  --restart unless-stopped \
  notkudu/xeroweb
```

**Build**
```
git clone https://gitlab.com/notkudu/xeroweb/
cd xeroweb
docker build -t xeroweb:latest .
```

for more info check [docker webtop](https://github.com/linuxserver/docker-webtop/blob/master/README.md)
